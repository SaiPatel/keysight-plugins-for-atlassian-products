package com.keysight.include.content.helpers;

import com.atlassian.confluence.pages.Page;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LikeUpdate
{
    @XmlElement private Page page = null;
    @XmlElement private String pageId = null;
    @XmlElement private String likeCount = "?";
    @XmlElement private String userLikePreference = "not-liked";


    public LikeUpdate( ){
        this.Initialize();
    }

    public LikeUpdate( Page page, String likeCount, boolean userLikesPage ){
        this.Initialize();
        this.DiscoverPageInfo(page);

        this.setLikeCount( likeCount );

        if( userLikesPage )
        {
            this.userLikePreference = "liked";
        }
        else
        {
            this.userLikePreference = "not-liked";
        }
    }

    private void Initialize(){
    }

    private void DiscoverPageInfo(Page page)
    {
        if( page != null )
        {
            this.setPage( page );
            this.setPageId( page.getIdAsString() );
        }
    }

    public void setPage( Page page ){
        this.page = page;
    }
    public Page getPage(){
        return this.page;
    }

    public void setPageId( String pageId ){
        this.pageId = pageId;
    }
    public String getPageId(){
        return this.pageId;
    }

    public void setLikeCount( String likeCount ){
        this.likeCount = likeCount;
    }
    public String getLikeCount(){
        return this.likeCount;
    }

    public void setUserLikePreference( String likePreference ){
        this.userLikePreference = likePreference;
    }
    public String getUserLikePreference(){
        return this.userLikePreference;
    }

    public int getLikeCountAsInt()
    {
        int count = 0;
        try{
            count = Integer.parseInt( this.likeCount );
        }
        catch( Exception exception )
        {
        }
        return 0;
    }

    public boolean userLikesPage()
    {
        boolean flag = false;
        if( this.userLikePreference == "liked")
        {
            flag = true;
        }
        return flag;
    }
}
