package com.keysight.mathjax.helpers;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.keysight.mathjax.rest.RestAdminConfigService;
import org.apache.commons.validator.routines.UrlValidator;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

// Helper functions for use throughout the plugin
@Component
public class PluginHelper {
    private final PluginSettingsFactory pluginSettingsFactory;
    private final TransactionTemplate transactionTemplate;
    private String m_url;

    @Autowired
    public PluginHelper(PluginSettingsFactory pluginSettingsFactory,
                        TransactionTemplate transactionTemplate ){
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.transactionTemplate = transactionTemplate;

        PluginSettings pluginSettings = this.pluginSettingsFactory.createGlobalSettings();
        UrlValidator urlValidator = new UrlValidator();
        String url;

        this.setUrl( Constants.DEFAULT_URL );

        try {
            url = (String) pluginSettings.get(RestAdminConfigService.Config.class.getName() + ".url");
            if (urlValidator.isValid(url)) {
                this.setUrl( url );
            }
        } catch (Exception exception) { }
    }

    public String getUrl(){
        return this.m_url;
    }

    public void setUrl( final String url ){
        this.m_url = url;
        transactionTemplate.execute(new TransactionCallback() {
            public Object doInTransaction() {
                PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
                pluginSettings.put(RestAdminConfigService.Config.class.getName() + ".url", url);
                return null;
            }
        });
    }
}
